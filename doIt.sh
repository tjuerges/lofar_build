#! /usr/bin/env bash

myName=$(basename ${0})
myName=${myName//.sh/}
myDir=${PWD}

host=$(hostname -s)

#build_type=gnu_opt
#build_type=gnucxx11_debug
#build_type=gnucxx11_opt
#build_type=clang_opt
build_type=clangcxx11_opt
case "${host}" in
    okeanos)
        build_type=clangcxx11_opt
        ;;
    dop407)
        build_type=gnucxx11_opt
        ;;
esac

pythonExecutable=$(which python3)
buildDir=${myDir}/source/${build_type}
installDir=${myDir}/install/${build_type}
lofarSrcDir=${HOME}/workspace.astron/gitsvn/LOFAR

qpidRootPath="${HOME}/soft/installed/qpid"
dalRootDir="${HOME}/soft/installed/DAL"
casacoreRootDir="${HOME}/soft/installed/casacore"

processors=$(nproc)
let jobs=${processors}+1
make_options="-j${jobs}"

cmake_options=""
cmake_options+=" -DDAL_ROOT_DIR=${dalRootDir}"
cmake_options+=" -DQPID_ROOT_DIR=${qpidRootPath}"
cmake_options+=" -DDPYTHON_EXECUTABLE=${pythonExecutable}"
cmake_options+=" -DCASACORE_ROOT_DIR=${casacoreRootDir}"
cmake_options+=" -DUSE_OPENMP=ON"
cmake_options+=" -DUSE_QPID=OFF"
cmake_options+=" -DUSE_LOG4CPLUS=OFF"
cmake_options+=" -DBUILD_TESTING=ON"
case "${host}" in
    okeanos)
        cmake_options+=" -DUSE_OPENMP=OFF"
        ;;
    dop407)
        ;;
esac

case ${myName} in
    build_install)
        if [ ${#} -gt 0 ]; then
            packages=${*}
        else
            printf "You need to specify at least one package!\n"
            exit 1
        fi
        mkdir -p ${buildDir} ${installDir}
        cd ${buildDir}
        cmake -DCMAKE_INSTALL_PREFIX="${installDir}" -DBUILD_PACKAGES="${packages}" ${cmake_options} ${lofarSrcDir}
        make ${make_options}
        make ${make_options} install
        ;;
    build)
        if [ ${#} -gt 0 ]; then
            packages=${*}
        else
            printf "You need to specify at least one package!\n"
            exit 1
        fi
        mkdir -p ${buildDir}
        cd ${buildDir}
        cmake -DBUILD_PACKAGES="${packages}" ${cmake_options} ${lofarSrcDir}
        make ${make_options}
        ;;
    clean)
        cd ${buildDir}
        make ${make_options} clean
        ;;
    distclean)
        rm -rf ${buildDir} ${installDir}
        rm -rf source install Testing
        ;;
    test)
        if [ ${#} -gt 0 ]; then
            tests=${*}
        else
            printf "You need to specify at least one test!\n"
            exit 1
        fi
        cd ${buildDir}
        ctest -V -R ${tests}
        ;;
    make)
        if [ ${#} -gt 0 ]; then
            targets=${*}
        else
            printf "You need to specify at least one target!\n"
            exit 1
        fi
        cd ${buildDir}
        make ${targets}
esac

