#! /usr/bin/env python3

import argparse
import sys


parser = argparse.ArgumentParser(description = "Byte compile a single Python3 file or all Python3 files in a directory and its subdirectories.")
parser.add_argument("--file", dest = "file", required = False, nargs = "+", type = str, help = "Python3 file to byte-compile")
parser.add_argument("--directory", dest = "directory", required = False, nargs = "+", type = str, help = "Directory and subdirectories containing Python3 files to byte-compile")
args = parser.parse_args()

file = None
directory = None

if args.directory is not None:
    directory = args.directory[0]
    print("Byte-compiling all files in directory %s..." % (directory))
    try:
        import compileall
        compileall.compile_dir(directory, maxlevels = 999999, force = True, quiet = 1, workers = 8)
    except Exception as ex:
        print("Caught an exception:  %s" % (str(ex)))
elif args.file is not None:
    file = args.file[0]
    print("Byte-compiling %s..." % (file))
    #try:
    import py_compile
    py_compile.compile(file)
    #except Exception as ex:
    #    print("Caught an exception:  %s" % (str(ex)))

sys.exit(0)

